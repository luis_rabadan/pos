create table llx_pos_cash_per_user
(
  rowid           	integer AUTO_INCREMENT PRIMARY KEY,
  entity            integer  DEFAULT 1 NOT NULL,
  fk_user       	integer,
  fk_terminal       integer,
  amount            double(24,8),
  datec				datetime,
  tipo      		tinyint
)ENGINE=innodb;