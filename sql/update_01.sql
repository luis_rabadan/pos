ALTER TABLE llx_pos_ticket ADD fk_place integer DEFAULT 0 AFTER fk_soc;
ALTER TABLE llx_pos_ticket CHANGE fk_place fk_place INT( 11 ) NULL;  
ALTER TABLE llx_pos_ticketdet ADD note TEXT NULL; 
ALTER TABLE llx_pos_cash ADD barcode TINYINT NOT NULL DEFAULT '0' AFTER tactil; 
ALTER TABLE llx_pos_ticketdet ADD localtax1_type INT NULL AFTER localtax1_tx;
ALTER TABLE llx_pos_ticketdet ADD localtax2_type INT NULL AFTER localtax2_tx;  
CREATE TABLE llx_pos_facture(rowid integer AUTO_INCREMENT PRIMARY KEY, fk_cash integer NOT NULL; fk_facture integer NOT NULL);
ALTER TABLE llx_pos_facture ADD fk_control_cash INT NULL;
ALTER TABLE llx_pos_facture ADD fk_place INT NULL;
ALTER TABLE llx_pos_facture ADD UNIQUE INDEX idx_facture_uk_facnumber (fk_facture);
ALTER TABLE llx_pos_cash ADD fk_modepaybank_extra INT NULL;
ALTER TABLE llx_pos_cash ADD fk_paybank_extra INT NULL;
ALTER TABLE llx_pos_cash ADD fk_modepaybank_extra2 INT NULL;
ALTER TABLE llx_pos_cash ADD fk_paybank_extra2 INT NULL;
ALTER TABLE llx_pos_cash ADD fk_modepaybank_extra3 INT NULL;
ALTER TABLE llx_pos_cash ADD fk_paybank_extra3 INT NULL;
ALTER TABLE llx_pos_cash ADD fk_modepaybank_extra4 INT NULL;
ALTER TABLE llx_pos_cash ADD fk_paybank_extra4 INT NULL;
ALTER TABLE llx_pos_cash ADD fk_modepaybank_extra5 INT NULL;
ALTER TABLE llx_pos_cash ADD fk_paybank_extra5 INT NULL;
ALTER TABLE llx_pos_control_cash ADD  ref varchar(30) NOT NULL;
ALTER TABLE llx_pos_facture ADD customer_pay double(24,8) DEFAULT 0;
ALTER TABLE llx_pos_ticket CHANGE fk_cash fk_cash integer NOT NULL;
ALTER TABLE llx_pos_cash ADD printer_name varchar(30) NULL;
-- Arqueo y Cierre
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_teor double(24,8); -- Cuenta bancaria por default
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_real double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_diff double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra_teor double(24,8);-- Cuenta bancaria extra
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra_real double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra_diff double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra2_teor double(24,8);-- Cuenta bancaria extra2
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra2_real double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra2_diff double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra3_teor double(24,8);-- Cuenta bancaria extra3
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra3_real double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra3_diff double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra4_teor double(24,8);-- Cuenta bancaria extra4
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra4_real double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra4_diff double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra5_teor double(24,8);-- Cuenta bancaria extra5
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra5_real double(24,8);
ALTER TABLE llx_pos_control_cash ADD COLUMN fk_paybank_extra5_diff double(24,8);
