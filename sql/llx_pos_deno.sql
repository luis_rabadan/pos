-- Tabla para el registro de la cantidad de dinero que se entrega en denominaciones

create table llx_pos_deno
(
  rowid           	integer AUTO_INCREMENT PRIMARY KEY,
  entity            integer  DEFAULT 1 NOT NULL,
  fk_control_cash	integer,
  qty		        integer,
  den		        double(24,8)
)ENGINE=innodb;